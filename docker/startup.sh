#!/bin/bash
#docker container run --rm  -e mujoco_env="FetchPickAndPlace-v1" -e learn_alg="normal" -e buffer_type="energy" -e log_tag="energy1" -v /home/nrp/Desktop/hgg_poet1:/home/user/hgg_test --name hgg_docker_work_instance_1 -it erdiphd/hgg_test

# sudo /usr/bin/supervisord -c /etc/supervisor/supervisord.conf  > /dev/null 2>&1 &
# /home/user/.conda/envs/HGG/bin/python home/user/HGG/train.py
# /usr/bin/bash
# source /home/user/conda/bin/activate
cd home/user/hgg
sudo chown -R user:user /home/user/hgg/
python train.py --env ${mujoco_env} --learn ${learn_alg} --buffer_type ${buffer_type} --goal ${goal} --tag ${log_tag}
# /bin/bash